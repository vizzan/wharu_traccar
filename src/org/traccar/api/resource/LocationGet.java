package org.traccar.api.resource;

import org.traccar.Context;
import org.traccar.api.BaseResource;
import org.traccar.model.Position;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Collection;

/**
 * Created by Net on 7/14/2016.
 */
@Path("getLocation")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LocationGet extends BaseResource {
    @PermitAll
    @GET
    public String get(@QueryParam("mobileno") String mobileno)
            throws SQLException
    {
        long deviceId = Context.getUserStatusManager().getDeviceId(mobileno);
        int isActive= Context.getUserStatusManager().isActive(mobileno);
        String locationlist="";
        if(isActive==0)
        {
            locationlist="Mobile number not registered or not active, 0";
        }
        else {
            locationlist = Context.getUserStatusManager().getPositions(deviceId);
        }
        return  locationlist;

    }
}
