package org.traccar.api.resource;

import org.traccar.Context;
import org.traccar.api.BaseResource;
import org.traccar.model.Device;
import org.traccar.model.Position;
import org.traccar.model.UserStatus;
import org.traccar.web.JsonConverter;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by Net on 7/12/2016.
 */
@Path("registerMobDevice")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RegistrationResource  extends BaseResource
{
    @PermitAll
    @GET
    public Device get(
            @QueryParam("mobileno") long mobileno, @QueryParam("countryCode") String countryCode, @QueryParam("DisplayName") String DisplayName
            , @QueryParam("OS") String OS, @QueryParam("deviceToken") String deviceToken)
            throws SQLException {
        Device device=new Device();
        device.setUniqueId(countryCode + mobileno);
        device.setName(DisplayName);
        device.setLastUpdate(new Date());
        /* if device is already exist */

        long deviceId=Context.getUserStatusManager().getDevices(device);
        device.setId(deviceId);
        /*******************************/
        if( deviceId==0l) {
            device.setOperation("INSERT");
            Context.getUserStatusManager().addDevice(device);
            //Context.getDataManager().linkDevice(1l, device.getId());
            deviceId=Context.getUserStatusManager().getDevices(device);
            Context.getUserStatusManager().addAppDeviceEntryReg(deviceId, OS, deviceToken);
        }
        else {
            device.setOperation("UPDATE");
            try {
                Context.getUserStatusManager().updateDevice(device);
               // Context.getDataManager().unlinkDevice(1l, device.getId());
              //  Context.getDataManager().linkDevice(1l, device.getId());
              //  UserStatus userStatus = new UserStatus();
              //  userStatus.setTo(device.getUniqueId());
               // userStatus = Context.getUserStatusManager().getUserStatus(userStatus);
                Context.getUserStatusManager().updateAppDeviceEntryReg(device.getId(), OS, deviceToken);

            }
            catch (Exception e){e.printStackTrace();}
        }

        return device;
    }

}
