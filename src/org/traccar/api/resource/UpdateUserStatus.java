package org.traccar.api.resource;

import org.traccar.Context;
import org.traccar.api.BaseResource;
import org.traccar.model.UserStatus;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
@Path("updateUserStatus")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)

/**
 * Created by Net on 7/15/2016.
 */
public class UpdateUserStatus extends BaseResource{
    @PermitAll
    @GET
    public String getUserStatus(@QueryParam("from") String from, @QueryParam("to") String to, @QueryParam("notifyBy") String notifyBy) {

        String status="";
        int fromDeviceId=Context.getUserStatusManager().getDeviceId(from);
        int toDeviceId=Context.getUserStatusManager().getDeviceId(to);
        try {
            Context.getUserStatusManager().updateNotification(fromDeviceId, toDeviceId, notifyBy);
            status="SMS Status is updated.";
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return status;

    }
}



