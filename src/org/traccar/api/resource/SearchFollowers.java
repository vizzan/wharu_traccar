package org.traccar.api.resource;

import org.traccar.Context;
import org.traccar.api.BaseResource;
import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
@Path("getFollowers")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)

/**
 * Created by Net on 7/15/2016.
 */
public class SearchFollowers extends BaseResource{
    @PermitAll
    @GET
    public String getFollowers(@QueryParam("mobileno") String mobileno) {

        String status="";
        try
        {
            long deviceId = Context.getUserStatusManager().getDeviceId(mobileno);
            status=Context.getUserStatusManager().getFollowers(deviceId);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return status;

    }
}



