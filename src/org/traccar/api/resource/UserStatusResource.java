package org.traccar.api.resource;

import org.traccar.Context;
import org.traccar.api.BaseResource;
import org.traccar.database.DataManager;
import org.traccar.model.UserStatus;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.*;

import javax.ws.rs.core.MediaType;


/**
 * Created by Usman on 11-07-2016.
 */
@Path("checkUserStatus")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public class UserStatusResource extends BaseResource {

    @PermitAll
    @POST
    public UserStatus getUserStatus(@FormParam("from") String from, @FormParam("to") String to) {
        UserStatus userStatus = new UserStatus();
        userStatus.setFrom(from);
        userStatus.setTo(to);
        userStatus = Context.getUserStatusManager().getUserStatus(userStatus);
        return userStatus;
    }

    @PermitAll
    @GET
    public UserStatus getDeviceStatus(@QueryParam("from") String from, @QueryParam("to") String to) {
        UserStatus userStatus = new UserStatus();
        userStatus.setFrom(from);
        userStatus.setTo(to);
        userStatus = Context.getUserStatusManager().getUserStatus(userStatus);
        return userStatus;
    }

}
