package org.traccar.api.resource;

import org.traccar.Config;
import org.traccar.Context;
import org.traccar.api.BaseResource;
import org.traccar.helper.DateBuilder;
import org.traccar.model.Device;
import org.traccar.model.Position;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import java.sql.SQLException;

/**
 * Created by Net on 7/14/2016.
 */
@Path("setLocation")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LocationSet extends BaseResource
{
    @PermitAll
    @GET

    public String get(@javax.ws.rs.core.Context HttpServletRequest request,@QueryParam("mobileno") String mobileno, @QueryParam("lat") Double latCode, @QueryParam("lon") Double longCode,
                      @QueryParam("date") String date,@QueryParam("time") String time,@QueryParam("altitude") Double altitude,@QueryParam("speed") Double speed,@QueryParam("heading") Double course)
    throws SQLException
    {
      String status="";
        int isActive= Context.getUserStatusManager().isActive(mobileno);
        if(isActive==0)
            status="Mobile number not registered or not active, 0";
        else
        {
           try {
               long deviceId = Context.getUserStatusManager().getDeviceId(mobileno);
               String deviceTime=date+" "+time;
               int year = Integer.parseInt(date.substring(4));
               int month = Integer.parseInt(date.substring(2, 4));
               int day = Integer.parseInt(date.substring(0, 2));
               int localHours = Integer.parseInt(time.substring(0, 2));
               int localMinutes = Integer.parseInt(time.substring(2, 4));
               int localsec = Integer.parseInt(time.substring(4, 6));
               int localMill = 0;
               DateBuilder dateBuilder = new DateBuilder().setDate(year, month, day);
               dateBuilder.setTime(localHours, localMinutes, localsec, localMill);
               Position position = new Position();
               position.setLatitude(latCode);
               position.setProtocol("wharru");
               position.setLongitude(longCode);
               position.setDeviceId(deviceId);
               position.setDeviceTime(dateBuilder.getDate());
               position.setFixTime(dateBuilder.getDate());
               position.setValid(true);
               position.setAltitude(altitude);
               position.setSpeed(speed);
               position.setCourse(course);
               String ipAddress = request.getRemoteHost();
               System.out.print(ipAddress);
               position.set(Position.KEY_IP, ipAddress);
               Context.getUserStatusManager().addLocation(position);
               status="Data saved successfully, 1";
           }
           catch (Exception e)
           {
               e.printStackTrace();
               status="Error During Insertion of location";
           }
        }
        return status;
    }

}
