/*
 * Copyright 2016 Anton Tananaev (anton.tananaev@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.traccar.protocol;

import org.jboss.netty.channel.Channel;
import org.traccar.BaseProtocolDecoder;
import org.traccar.helper.DateBuilder;
import org.traccar.helper.Parser;
import org.traccar.helper.PatternBuilder;
import org.traccar.model.Position;

import java.net.SocketAddress;
import java.util.Date;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppelloProtocolDecoder extends BaseProtocolDecoder {

    public AppelloProtocolDecoder(AppelloProtocol protocol) {
        super(protocol);
    }

    private static final Pattern PATTERN_HANDSHAKE = new PatternBuilder()
            .number("##,imei:(d+),A")
            .compile();

    private static final Pattern PATTERN = new PatternBuilder()
            .text("FOLLOWIT,")                   // brand
            .number("(d+),")                     // imei
            .groupBegin()
            .number("(dd)(dd)(dd)")              // date
            .number("(dd)(dd)(dd).?d*,")         // time
            .or()
            .text("UTCTIME,")
            .groupEnd()
            .number("(-?d+.d+),")                // latitude
            .number("(-?d+.d+),")                // longitude
            .number("(d+),")                     // speed
            .number("(d+),")                     // course
            .number("(d+),")                     // satellites
            .number("(-?d+),")                   // altitude
            .expression("([FL]),")               // gps state
            .any()
            .compile();

    /* new Code added by deepak *********************************/

    private static final Pattern PATTERNSNEW = new PatternBuilder()
            .expression("[\\$],")              // start of string �$�  Fixed
            .number("[0-9]{1,4},")     // 85 86 87 --- Fixed Frame Type
            .number("(d{8}),")     // Device Number  Unique 8 chars
            .number("[0-9a-fA-F]{4},")      // Frame number   Hex � auto increment
            .number("(d{6}),")      // Time: HHMMSS
            .number("(d{6}),")      // Date*/
            .expression("([A|V|X]),").optional()    //Gps-Fix A for Fix   X,V for No Fix
            .number("(d+)(dd.d+),")              // latitude (ddmm.mmmm)
            .expression("([N|S]),").optional()    //Depending upon Hemisphere
            .number("(d+)(dd.d+),")              // longitude (dddmm.mmmm)
            .expression("([E|W]),").optional()
            .number("[0-9a-fA-F][0-9a-fA-F],")              // Speed in In HEX
            .number("[0-9]{3},")              // Bearing
            .number("(d+").text(".").number("d+),")     //Distance
            .number("[0-9]{1,4},")                  // Device Status 58 , 59 , 60     61 � 1=BD    61 � 2=IDLE    61 � 3 = MOV
            .number("[0-9]{1,4},")                  // Comm-Status    62- Unused    63- Unused    64 � 2 = Communicating    65- Unused
                    //.number("[0-9 ]+{8},")                 // Route id
            .number("[0-9 ]+ ,")                 // Route id
            .number("[0-9a-fA-F][0-9a-fA-F],")    // Trip#
            .number("[0-9a-fA-F][0-9a-fA-F],")    // Sequence No#
            .number("[0]{9},")                   //Available as zeros
            .number("(d+").text(".").number("d+),") //Cumulative Distance length 7 99999.9
            .number("[0-9a-fA-F][0-9a-fA-F],")                       //Ignition Counter
            .number("[+0-9]{4},")                     //Temperature
            .expression("([a-zA-Z0-9\\s]+),")                 // Start Pointer
            .expression("([a-zA-Z0-9\\s]+),")                   // End Pointer
            .number("(d+").text(".").number("d+),")  //Input Supply Voltage in mV
            .number("(d+").text(".").number("d+),") //Internal Battery Voltage in mV
            .number("[01]{2},")                    //01 -  ACC IGN ONN    00 -  ACC IGN OFF
            .number("[0-9a-fA-F][0-9a-fA-F],")         //Auto increment from 00 to FF
            .number("[0-9]{2},")          //Checksum*/
            .expression("\\*")        // end of String
            .any()
            .compile();

    @Override
    protected Object decode(
            Channel channel, SocketAddress remoteAddress, Object msg) throws Exception {
        String sentence = (String) msg;

        try{

            /*if (sentence.contains("$") && channel != null) {
                String response = "";
                response += sentence.substring(124, sentence.lastIndexOf('*'));
                response += '\0';
                channel.write(response); // heartbeat response
            }*/
            // Send response #1
           /* if (sentence.contains("$")) {
                if (channel != null) {
                    channel.write("LOAD", remoteAddress);
                    Parser handshakeParser = new Parser(PATTERNSNEW, sentence);
                    if (handshakeParser.matches()) {
                        identify(handshakeParser.next(), channel, remoteAddress);
                    }
                }
                return null;
            }*/

            // Send response #2
           /* if (!sentence.isEmpty() && Character.isDigit(sentence.charAt(0))) {
                if (channel != null) {
                    channel.write("ON", remoteAddress);
                }
                int start = sentence.indexOf("imei:");
                if (start >= 0) {
                    sentence = sentence.substring(start);
                } else {
                    return null;
                }
            }*/
            Position position = new Position();
            //Parser parser = new Parser(PATTERNSNEW, (String) msg);
        /*if (!parser.matches()) {
            return null;
        }
        else{
            System.out.println("Pattern Match");
        }*/

            String value = (String) msg;
            System.out.println("Pattern Value" + value);
        /*if (!parser.matches()) {
            return null;
        }else{
            System.out.println("Pattern Match");
        }*/
            Parser parser = new Parser(PATTERNSNEW, (String) msg);
            String datavalue = "";
            String pattern = "";
            //1 Start Character
            datavalue = value.substring(0, 1);
            System.out.println(datavalue);
            if (!"\\$".equals(datavalue)) {
                //System.out.println("doller symbol match");
            } else {
                //System.out.println("not match");
            }
            //2 -3 Frame Type
            datavalue = value.substring(1, 3);
            System.out.println(datavalue);
            pattern = "[0-9]{1,4}";
            // Create a Pattern object
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(datavalue);
            if ("85".equals(datavalue) || "86".equals(datavalue) || "87".equals(datavalue) || "  ".equals(datavalue)) {
                // System.out.println("Frame Type Matched");
            } else {
                //System.out.println("Not matched");
            }
            //4-11 Device Number
            datavalue = value.substring(3, 11);
            //position.setDeviceId(Long.parseLong(datavalue));
            if (!identify(datavalue, channel, remoteAddress)) {
                return null;
            }
            position.setDeviceId(getDeviceId());
            System.out.println("Device Number" + datavalue);
            if (value.contains(datavalue)) {
                //System.out.println("Device ID matched");
            } else {
                //System.out.println("Device not matched");
            }
            //11-15 Frame Number
            datavalue = value.substring(11, 15);
            System.out.println("Frame Number" + datavalue);
            //pattern = "[0-9a-fA-F]{4}";
            //System.out.println("matcher"+Pattern.matches("[0-9a-fA-F]{4}", datavalue));
            // Pattern pattern1 = Pattern.compile(pattern);
            //Matcher matcher = pattern1.matcher(datavalue);
            //boolean matches = matcher.matches();
            if (value.contains(datavalue)) {
                // System.out.println("Frame Number Matched");
            } else {
                //System.out.println("Frame Number not matched");
            }
            //15-21 Time
            datavalue = value.substring(15, 21);
            String time = datavalue;

            int localHours = Integer.parseInt(time.substring(0, 2));
            int localMinutes = Integer.parseInt(time.substring(2, 4));
            int localsec = Integer.parseInt(time.substring(4, 6));
            int localMill = 0;
            DateBuilder dateBuilder = new DateBuilder();
            dateBuilder.setTime(localHours, localMinutes, localsec, localMill);

            System.out.println("Time" + datavalue);
            // 21-27 Date
            datavalue = value.substring(21, 27);
            String date = datavalue;
            int year = Integer.parseInt(date.substring(4));
            int month = Integer.parseInt(date.substring(2, 4));
            int day = Integer.parseInt(date.substring(0, 2));
            dateBuilder.setDate(year, month, day);
            System.out.println("Date" + datavalue);
            //27-28 Gps-fix
            datavalue = value.substring(27, 28);
            System.out.println("Gps-fix" + datavalue);
            //28-36 Latitude
            datavalue = value.substring(28, 36);
            System.out.println("Latitude" + datavalue);
            String degree=datavalue.substring(0,2);
            String min=datavalue.substring(2,4);
            String sec=datavalue.substring(4);
            double decimal=Double.parseDouble(degree);
            double min_d=(Double.parseDouble(min))+(Double.parseDouble(sec)/1000);
            min_d=min_d/60;
            min_d=Double.parseDouble(degree)+min_d;
            position.setLatitude(min_d);
            //position.setLatitude(datavalue.nextCoordinate(Parser.CoordinateFormat.HEM_DEG_MIN_HEM));
            //position.setLatitude(Double.parseDouble(datavalue));
            //36-37 Lat-Hem
            datavalue = value.substring(36, 37);
            System.out.println("Lat-Hem" + datavalue);
            //37-46 Longitude
            datavalue = value.substring(37, 46);
            position.setLongitude(Double.parseDouble(datavalue));
            // position.setLongitude(parser.nextCoordinate(Parser.CoordinateFormat.HEM_DEG_MIN_HEM));
            // eg. 07717.3644 E is the DDMM.MMMM format

            // 077 --> degrees
            degree=datavalue.substring(0,3);
            min=datavalue.substring(3,5);
            sec=datavalue.substring(5);
            decimal=Double.parseDouble(degree);
            min_d=Double.parseDouble(min)+Double.parseDouble(sec)/1000;
            min_d=min_d/60;
            decimal=Double.parseDouble(degree)+min_d;
            position.setLongitude(decimal);
       /* 17 --> minutes
        .3644 --> minutes equals to sec/60


        decimal = degrees + minutes/60

        decimal = 77 + (17.3644 / 60)

        decimal = 77.28941*/
            System.out.println("Longitude" + datavalue);
            // 46-47 Long-Hem
            datavalue = value.substring(46, 47);
            System.out.println("Long-Hem" + datavalue);
            // 47-49 Speed
            datavalue = value.substring(47, 49);
            System.out.println("Speed" + datavalue);
            //49- 52 Bearing
            datavalue = value.substring(49, 52);
            System.out.println("Bearing" + datavalue);
            //52-57 Distance
            datavalue = value.substring(52, 57);
            System.out.println("Distance" + datavalue);
            //57-61 Device Status
            datavalue = value.substring(57, 61);
            System.out.println("Device Status" + datavalue);
            //61-65 Comm-Status
            datavalue = value.substring(61, 65);
            System.out.println("Comm-Status" + datavalue);
            //65-73 Route-Id
            datavalue = value.substring(65, 73);
            System.out.println("Route-Id" + datavalue);
            //73-75 Trip
            datavalue = value.substring(73, 75);
            System.out.println("Trip" + datavalue);
            //75-77 Sequence No#
            datavalue = value.substring(75, 77);
            System.out.println("Sequence No#" + datavalue);
            //77-86 Filter all 0's
            datavalue = value.substring(77, 86);
            System.out.println("Filter all 0's" + datavalue);
            //86-93 Commulative Distance
            datavalue = value.substring(86, 93);
            System.out.println("Commulative Distance" + datavalue);
            //93-95 Ignition counter
            datavalue = value.substring(93, 95);
            System.out.println("Ignition counter" + datavalue);
            //95-99 Temperature
            datavalue = value.substring(95, 99);
            System.out.println("Temperature" + datavalue);
            //99-104 Start Pointer
            datavalue = value.substring(99, 104);
            System.out.println("Start Pointer" + datavalue);
            //104-109 End Pointer
            datavalue = value.substring(104, 109);
            System.out.println("End Pointer" + datavalue);
            //109-113 VIN
            datavalue = value.substring(109, 113);
            System.out.println("VIN" + datavalue);
            //113-117 VBAT
            datavalue = value.substring(113, 117);
            System.out.println("VBAT" + datavalue);
            //117-119 ACC IGN STATUS
            datavalue = value.substring(117, 119);
            System.out.println("ACC IGN STATUS" + datavalue);
            //119-121 ACC-ON counter
            datavalue = value.substring(119, 121);
            System.out.println("ACC-ON counter" + datavalue);
            //121-123 Checksum
            datavalue = value.substring(121, 123);
            System.out.println("Checksum" + datavalue);
            //123-124 End Character
            datavalue = value.substring(123, 124);
            System.out.println("End Character" + datavalue);

            position.setProtocol(getProtocolName());


            // String fix = parser.next();

            position.setValid(true);
            position.setFixTime(new Date());
            position.setDeviceTime(new Date());
        /*if (parser.hasNext(6)) {
            DateBuilder dateBuilder = new DateBuilder()
                    .setDate(parser.nextInt(), parser.nextInt(), parser.nextInt())
            dateBuilder.setTime(localHours, localMinutes, localsec, localMill);
            position.setTime(dateBuilder.getDate());
        } else {
            getLastLocation(position, null);
        }*/

      /*  position.setLatitude(parser.nextDouble());
        position.setLongitude(parser.nextDouble());
        position.setSpeed(parser.nextDouble());
        position.setCourse(parser.nextDouble());

        position.set(Position.KEY_SATELLITES, parser.nextInt());

        position.setAltitude(parser.nextDouble());

        position.setValid(parser.next().equals("F"));*/

            return position;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
