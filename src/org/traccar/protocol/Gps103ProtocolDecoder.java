/*
 * Copyright 2012 - 2016 Anton Tananaev (anton.tananaev@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.traccar.protocol;

import com.sun.org.apache.xpath.internal.SourceTree;
import org.jboss.netty.channel.Channel;
import org.traccar.BaseProtocolDecoder;
import org.traccar.helper.DateBuilder;
import org.traccar.helper.Parser;
import org.traccar.helper.PatternBuilder;
import org.traccar.model.Position;

import java.net.SocketAddress;
import java.util.regex.Pattern;

public class Gps103ProtocolDecoder extends BaseProtocolDecoder {

    public Gps103ProtocolDecoder(Gps103Protocol protocol) {
        super(protocol);
    }

    private static final Pattern PATTERN = new PatternBuilder()
            .text("imei:")
            .number("(d+),")                     // imei
            .expression("([^,]+),")              // alarm
            .number("(dd)/?(dd)/?(dd) ?")        // local date
            .number("(dd):?(dd)(?:dd)?,")        // local time
            .expression("([^,]+)?,")             // rfid
            .expression("[FL],")                 // full / low
            .groupBegin()
            .number("(dd)(dd)(dd).(d+)")         // time utc (hhmmss.sss)
            .or()
            .number("(?:d{1,5}.d+)?")
            .groupEnd()
            .text(",")
            .expression("([AV]),")               // validity
            .expression("([NS]),").optional()
            .number("(d+)(dd.d+),")              // latitude (ddmm.mmmm)
            .expression("([NS]),").optional()
            .expression("([EW]),").optional()
            .number("(d+)(dd.d+),")              // longitude (dddmm.mmmm)
            .expression("([EW])?,").optional()
            .number("(d+.?d*)?,?")               // speed
            .number("(d+.?d*)?,?")               // course
            .number("(d+.?d*)?,?")               // altitude
            .expression("([^,;]+)?,?")
            .expression("([^,;]+)?,?")
            .expression("([^,;]+)?,?")
            .expression("([^,;]+)?,?")
            .expression("([^,;]+)?,?")
            .any()
            .compile();

    private static final Pattern PATTERN_NETWORK = new PatternBuilder()
            .text("imei:")
            .number("(d+),")                     // imei
            .expression("[^,]+,")                // alarm
            .number("d*,,")
            .text("L,,,")
            .number("(x+),,")                    // lac
            .number("(x+),,,")                   // cid
            .any()
            .compile();

    private static final Pattern PATTERN_HANDSHAKE = new PatternBuilder()
            .number("##,imei:(d+),A")
            .compile();

    private static final Pattern PATTERN_OBD = new PatternBuilder()
            .text("imei:")
            .number("(d+),")                     // imei
            .expression("OBD,")                  // type
            .number("(dd)(dd)(dd)")              // date
            .number("(dd)(dd)(dd),")             // time
            .number("(d+),")                     // odometer
            .number("(d+.d+)?,")                 // fuel instant
            .number("(?:d+.d+)?,")               // fuel average
            .number("(d+),")                     // hours
            .number("(d+),")                     // speed
            .number("d+.?d*%,")                  // power load
            .number("(d+),")                     // temperature
            .number("(d+.?d*%),")                // throttle
            .number("(d+),")                     // rpm
            .number("(d+.d+),")                  // battery
            .number("[^,]*,")                    // dtc 1
            .number("[^,]*,")                    // dtc 2
            .number("[^,]*,")                    // dtc 3
            .number("[^,]*")                     // dtc 4
            .any()
            .compile();
    /* deepaK pATTERN ****************************
    private static final Pattern PATTERNSELF = new PatternBuilder()
            .expression("[\\$]")              // start of string
            .expression("[nor|rmv|ebl|ibl|tmp|smt|btt|ion|iof]+,")
            .expression("[LH],")          // Data type: L-Live H-History
            .number("(d{15}),")     // imei
            .number("(d{8}),")      // Date: DDMMYYYY
            .number("(d{6}),")      // Time: HHMMSS
            /*.number("[0-4{1}],")    // GPRS/GPS Status: 0–None, 1–GPRS Fix, 2–GPS Fix, 3–GNSS Fix, 4–3D Fix(GPS,GNSS)
            .number("(d{4}").text(".").number("d{4}),")   // Lat: DDMM.MMMM
            .expression("[NS],")          // Lat Direction: N/S > North/South
            .number("(d{5}").text(".").number("d{4}),")   // Long: DDDMM.MMMM format
            .expression("[EW],")          // Long Direction: E/W >> East/West
            .number("(d+").text(".").number("d+),")    // Speed in Km/h : xxx.x resolution to 100m
            .number("(d+").text(".").number("d+),")
            .number("(d+").text(".").number("d+")
            .or()
            .number("d+),")
            .number("(d+").text(".").number("d+),")
            .number("(d{1,3}),")
            .number("(d{1,3}),")
            .expression("([a-zA-Z\\s]+),")    // Network Operator
            .number("(d{1,2}),")        // GSM Strength
            .expression("([a-zA-Z0-9\\s]+),")    // Cell Id
            .expression("([01]),")          // Ignition Status: 1-On, 0–Off
            .number("(d").text(".").number("d+),")
            .number("(d+").text(".").number("d),")
            .number("(d+),")
            .number("(d+),")
            .number("([01]{4}),")
            .number("(d+").text(".").number("d+),")
            .number("(d+").text(".").number("d+),")
            .number("(d+").text(".").number("d+),")
            .expression("([$#\\w\\d\\.]+),")      // Software Version*/
         /*   .expression("(#)")        // end of String
            .any()
            .compile();
    ***************************************/
    private static final Pattern PATTERNSELF = new PatternBuilder()
            .expression("[\\$]")              // start of string
            .expression("[nor|rmv|ebl|ibl|tmp|smt|btt|ion|iof]+,")
            .expression("[LH],")          // Data type: L-Live H-History
            .number("(d{15}),")     // imei
            .number("(d{8}),")      // Date: DDMMYYYY
            .number("(d{6}),")      // Time: HHMMSS
            .number("[0-4{1}],")    // GPRS/GPS Status: 0–None, 1–GPRS Fix, 2–GPS Fix, 3–GNSS Fix, 4–3D Fix(GPS,GNSS)
             /*.number("(d+").text(".").number("d+),")   // Lat: DDMM.MMMM
            .expression("[NS],")          // Lat Direction: N/S > North/South
            .number("(d+").text(".").number("d+),")   // Long: DDDMM.MMMM format
            .expression("[EW],")          // Long Direction: E/W >> East/West*/
            .expression("([NS]),").optional()
            .number("(d+)(dd.d+),")              // latitude (ddmm.mmmm)
            .expression("([NS]),").optional()
            .expression("([EW]),").optional()
            .number("(d+)(dd.d+),")              // longitude (dddmm.mmmm)
            .expression("([EW])?,").optional()
            .number("(d+").text(".").number("d+),")    // Speed in Km/h : xxx.x resolution to 100m
            .number("(d+").text(".").number("d+),")     //Heading
            .number("(d+").text(".").number("d+),")     // Altitude in meters from see level
            .number("(d+").text(".").number("d+),")     // HDOP
            .number("[0-9]{1,2},")                       //Used SateLite
            .number("[0-9]{1,2},")                        //Visible Satelite
            .expression("([a-zA-Z\\s]+),")    // Network Operator
            .number("[0-9]{1,2},")                    // GSM Strength should be in 0-31 below 12 is not accected
            .expression("([a-zA-Z0-9\\s]+),")                  // cid
            .number("[01]{1},")                     // IGINITION SATUS (0/1),
            .number("(d").text(".").number("d+),")  //Internal Battery Voltage
            .number("(d+").text(".").number("d+),") //Internal Battery Voltage
            .number("[01]{8},")          //BITWISE DIGITAL INPUT STATUS (000000xx),
            .number("[01]{8},")          //BITWISE DIGITAL OUTPUT STATUS (000000xx),
            .number("[01]{4},")          //4 TAMPERING STATUS
            .number("(d+").text(".").number("d+),")  //1st Analog Input
            .number("(d+").text(".").number("d+),")  //2nd Analog Input
            .number("(d+").text(".").number("d+),")  //Trip Meter
            .expression("([$#\\w\\d\\.]+),")      // Software Version*/
            .expression("(#)")        // end of String
            .any()
            .compile();


    @Override
    protected Object decode(
            Channel channel, SocketAddress remoteAddress, Object msg) throws Exception {

        String sentence = (String) msg;

        // Send response #1
        if (sentence.contains("##")) {
            if (channel != null) {
                channel.write("LOAD", remoteAddress);
                Parser handshakeParser = new Parser(PATTERN_HANDSHAKE, sentence);
                if (handshakeParser.matches()) {
                    identify(handshakeParser.next(), channel, remoteAddress);
                }
            }
            return null;
        }

        // Send response #2
        if (!sentence.isEmpty() && Character.isDigit(sentence.charAt(0))) {
            if (channel != null) {
                channel.write("ON", remoteAddress);
            }
            int start = sentence.indexOf("imei:");
            if (start >= 0) {
                sentence = sentence.substring(start);
            } else {
                return null;
            }
        }

        Position position = new Position();
        position.setProtocol(getProtocolName());

        Parser parser = new Parser(PATTERN_NETWORK, sentence);
        if (parser.matches()) {

            if (!identify(parser.next(), channel, remoteAddress)) {
                return null;
            }
            position.setDeviceId(getDeviceId());

            getLastLocation(position, null);

            position.set(Position.KEY_LAC, parser.nextInt(16));
            position.set(Position.KEY_CID, parser.nextInt(16));

            return position;

        }

        parser = new Parser(PATTERN_OBD, sentence);
        if (parser.matches()) {

            if (!identify(parser.next(), channel, remoteAddress)) {
                return null;
            }
            position.setDeviceId(getDeviceId());

            DateBuilder dateBuilder = new DateBuilder()
                    .setDate(parser.nextInt(), parser.nextInt(), parser.nextInt())
                    .setTime(parser.nextInt(), parser.nextInt(), parser.nextInt());

            getLastLocation(position, dateBuilder.getDate());

            position.set(Position.KEY_ODOMETER, parser.nextInt());
            position.set(Position.KEY_FUEL, parser.next());
            position.set(Position.KEY_HOURS, parser.next());
            position.set(Position.KEY_OBD_SPEED, parser.next());
            position.set(Position.PREFIX_TEMP + 1, parser.next());
            position.set(Position.KEY_THROTTLE, parser.next());
            position.set(Position.KEY_RPM, parser.next());
            position.set(Position.KEY_BATTERY, parser.next());

            return position;

        }
        if (sentence.contains("$")) {
            parser = new Parser(PATTERNSELF, sentence);
            if (!parser.matches()) {
                return null;
            } else {
                System.out.println("Pattern match::::::::::::::::::::::::::::::::::::::>>>new Build");
                String imei = parser.next();
                if (!identify(imei, channel, remoteAddress)) {
                    return null;
                }
                position.setDeviceId(getDeviceId());

              /*  String alarm = parser.next();
                position.set(Position.KEY_ALARM, alarm);
                if (channel != null && alarm.equals("help me")) {
                    channel.write("**,imei:" + imei + ",E;", remoteAddress);
                }*/
                try {
                    String date = parser.next();
                    int year = Integer.parseInt(date.substring(4));
                    int month = Integer.parseInt(date.substring(2, 4));
                    int day = Integer.parseInt(date.substring(0, 2));
                    DateBuilder dateBuilder = new DateBuilder().setDate(year, month, day);

                    String time = parser.next();

                    int localHours = Integer.parseInt(time.substring(0, 2));
                    int localMinutes = Integer.parseInt(time.substring(2, 4));
                    int localsec = Integer.parseInt(time.substring(4, 6));
                    ;
                    int localMill = 0;

                    //String rfid = parser.next();
                    dateBuilder.setTime(localHours, localMinutes, localsec, localMill);
                    //  position.setGprsStatus(parser.next());
                    //position.setLatitude(parser.nextDouble());
                    // position.setLatitude(parser.nextCoordinate(Parser.CoordinateFormat.HEM_DEG_MIN_HEM));
                    // position.setLatDirection(parser.next());
                    //position.setLongitude(parser.nextDouble());
                    //  position.setLongitude(parser.nextCoordinate(Parser.CoordinateFormat.HEM_DEG_MIN_HEM));
                    position.setLatitude(parser.nextCoordinate(Parser.CoordinateFormat.HEM_DEG_MIN_HEM));
                    position.setLongitude(parser.nextCoordinate(Parser.CoordinateFormat.HEM_DEG_MIN_HEM));
                    position.setValid(true);
                    position.set(Position.KEY_HOURS, 10);
                    position.setSpeed(parser.nextDouble());
                    position.setCourse(parser.nextDouble());
                     position.setAltitude(parser.nextDouble());
                    //  position.setLongDirection(parser.next());
               /* if (alarm.equals("rfid")) {
                    position.set(Position.KEY_RFID, rfid);
                }*/

                 /*   String utcHours = parser.next();
                    String utcMinutes = parser.next();

                    dateBuilder.setTime(localHours, localMinutes, parser.nextInt(), parser.nextInt());

                    // Timezone calculation
                    if (utcHours != null && utcMinutes != null) {
                        int deltaMinutes = (localHours - Integer.parseInt(utcHours)) * 60;
                        deltaMinutes += localMinutes - Integer.parseInt(utcMinutes);
                        if (deltaMinutes <= -12 * 60) {
                            deltaMinutes += 24 * 60;
                        } else if (deltaMinutes > 12 * 60) {
                            deltaMinutes -= 24 * 60;
                        }
                        dateBuilder.addMinute(-deltaMinutes);
                    }*/
                    position.setTime(dateBuilder.getDate());

                    // position.setValid(parser.next().equals("A"));
                    //      position.setLatitude(parser.nextCoordinate(Parser.CoordinateFormat.HEM_DEG_MIN_HEM));
                    // position.setSpeed(parser.nextDouble());
                    //  position.setCourse(parser.nextDouble());
               /* position.setAltitude(parser.nextDouble());

                for (int i = 1; i <= 5; i++) {
                    position.set(Position.PREFIX_IO + i, parser.next());
                }*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return position;
            }
        }
        parser = new Parser(PATTERN, sentence);
        if (!parser.matches()) {
            return null;
        }

        String imei = parser.next();
        if (!identify(imei, channel, remoteAddress)) {
            return null;
        }
        position.setDeviceId(getDeviceId());

        String alarm = parser.next();
        position.set(Position.KEY_ALARM, alarm);
        if (channel != null && alarm.equals("help me")) {
            channel.write("**,imei:" + imei + ",E;", remoteAddress);
        }

        DateBuilder dateBuilder = new DateBuilder()
                .setDate(parser.nextInt(), parser.nextInt(), parser.nextInt());

        int localHours = parser.nextInt();
        int localMinutes = parser.nextInt();

        String rfid = parser.next();
        if (alarm.equals("rfid")) {
            position.set(Position.KEY_RFID, rfid);
        }

        String utcHours = parser.next();
        String utcMinutes = parser.next();

        dateBuilder.setTime(localHours, localMinutes, parser.nextInt(), parser.nextInt());

        // Timezone calculation
        if (utcHours != null && utcMinutes != null) {
            int deltaMinutes = (localHours - Integer.parseInt(utcHours)) * 60;
            deltaMinutes += localMinutes - Integer.parseInt(utcMinutes);
            if (deltaMinutes <= -12 * 60) {
                deltaMinutes += 24 * 60;
            } else if (deltaMinutes > 12 * 60) {
                deltaMinutes -= 24 * 60;
            }
            dateBuilder.addMinute(-deltaMinutes);
        }
        position.setTime(dateBuilder.getDate());

        position.setValid(parser.next().equals("A"));
        position.setLatitude(parser.nextCoordinate(Parser.CoordinateFormat.HEM_DEG_MIN_HEM));
        position.setLongitude(parser.nextCoordinate(Parser.CoordinateFormat.HEM_DEG_MIN_HEM));
        position.setSpeed(parser.nextDouble());
        position.setCourse(parser.nextDouble());
        position.setAltitude(parser.nextDouble());

        for (int i = 1; i <= 5; i++) {
            position.set(Position.PREFIX_IO + i, parser.next());
        }

        return position;
    }

}
