package org.traccar.database;

import liquibase.sql.Sql;
import org.json.JSONArray;
import org.json.JSONObject;
import org.traccar.model.Device;
import org.traccar.model.Position;
import org.traccar.model.UserStatus;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.xml.crypto.Data;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Usman on 12-07-2016.
 */
public class UserStatusManager {
    private DataManager dataManager;

    public UserStatusManager(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    public UserStatus getUserStatus(UserStatus userStatus) {
        QueryBuilder queryBuilder=null;
        try {
            int fromDeviceId = getDeviceId(userStatus.getFrom());
            int toDeviceId = getDeviceId(userStatus.getTo());
            if (fromDeviceId != 0) {
                queryBuilder = QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.selectDeviceStatus")).setString("uniqueid", userStatus.getTo()).setInteger("fromId", fromDeviceId);
                ResultSet resultSet1 = queryBuilder.getStatement().executeQuery();

                userStatus.setIsRequestMakerExist("Yes");
                if (resultSet1.next()) {
                    // int deviceId = resultSet.getInt(1);
                    String os = resultSet1.getString("os");
                    String deviceToken = resultSet1.getString("deviceToken");
                    if (deviceToken == null) {
                        userStatus.setIsUserRegisterd("No");
                    } else {
                        userStatus.setIsUserRegisterd("Yes");
                    }
                    boolean isSmsSent = resultSet1.getBoolean("isSmsSent");
                    int notificationRequestStatus = resultSet1.getInt("notiRequestStatus");
                    userStatus.setNotificationRequestStatus(notificationRequestStatus);

                    if (isSmsSent) {
                        userStatus.setIsSmsSent("Yes");
                    } else {
                        userStatus.setIsSmsSent("No");
                    }
                } else if (toDeviceId != 0) {
                    userStatus.setIsUserRegisterd("Yes");
                    userStatus.setIsSmsSent("NO");
                    storeSharingStatusEntry(fromDeviceId, toDeviceId);
                } else {
                    userStatus.setIsUserRegisterd("No");
                    userStatus.setIsSmsSent("NO");
                    addDeviceEntry(userStatus, fromDeviceId);

                }
            } else {
                userStatus.setIsRequestMakerExist("No");
            }


        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }finally {
            queryBuilder.closeConnection();

        }
        return userStatus;
    }

    public boolean addDeviceEntry(UserStatus userStatus, int fromDeviceId) {
        boolean flag = false;
        QueryBuilder queryBuilder=null;
        try {
            queryBuilder = QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.insertDeviceEntry"), true).setString("name", userStatus.getTo()).setString("uniqueid", userStatus.getTo());
            int affectedRow = queryBuilder.getStatement().executeUpdate();
            int devideId = 0;
            if (affectedRow > 0) {
                ResultSet resultSet = queryBuilder.getStatement().getGeneratedKeys();
                if (resultSet.next()) {
                    devideId = resultSet.getInt(1);
                }
            }
            if (devideId != 0) {
                addAppDeviceEntry(devideId);
                storeSharingStatusEntry(fromDeviceId, devideId);
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            queryBuilder.closeConnection();
        }
        return false;
    }

    public void addAppDeviceEntry(int deviceId) {
        QueryBuilder queryBuilder=null;
        try {
            queryBuilder = QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.insertAppDeviceEntry"), true).setInteger("deviceId", deviceId);
            int affectedRow = queryBuilder.getStatement().executeUpdate();
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }finally {
            queryBuilder.closeConnection();
        }
    }


    public void storeSharingStatusEntry(int fromId, int toId) {
        QueryBuilder queryBuilder=null;
        try {
            queryBuilder = QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.insertSharingStatusEntry")).setInteger("from", fromId).setInteger("to", toId);
            queryBuilder.getStatement().executeUpdate();
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        }finally {
            queryBuilder.closeConnection();
        }
    }

    public int getDeviceId(String uniqueId) {
        QueryBuilder queryBuildr=null;
        int fromDeviceId = 0;
        try {
            queryBuildr = QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.selectDeviceId")).setString("uniqueid", uniqueId);
            ResultSet resultSet = queryBuildr.getStatement().executeQuery();

            if (resultSet.next()) {
                fromDeviceId = resultSet.getInt("id");
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }finally {
            queryBuildr.closeConnection();
        }
        return fromDeviceId;
    }
    /****/
    public void addAppDeviceEntryReg(long deviceId,String os,String deviceTocken) {
        QueryBuilder queryBuilder=null;
        try
        {
            queryBuilder = QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.insertAppDeviceEntryReg")).setLong("deviceId", deviceId)
                    .setString("os", os).setString("deviceToken", deviceTocken);
            int affectedRow = queryBuilder.getStatement().executeUpdate();
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        finally {
            queryBuilder.closeConnection();
        }
    }
    public void updateAppDeviceEntryReg(long deviceId,String os,String deviceTocken) {
        QueryBuilder queryBuilder=null;
        try
        {
            queryBuilder = QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.updateAppDeviceEntryReg"))
                    .setString("os", os).setString("deviceToken", deviceTocken).setLong("deviceId", deviceId);
            int affectedRow = queryBuilder.getStatement().executeUpdate();
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }finally {
            queryBuilder.closeConnection();
        }
    }
    public void updateDevice(Device device) throws SQLException {
        QueryBuilder queryBuilder=null;
        try {
            queryBuilder=QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.updateDevice"))
                    .setObject(device);
            queryBuilder.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            queryBuilder.closeConnection();
        }
    }
    public void addDevice(Device device) throws SQLException {
        QueryBuilder queryBuilder=null;
        try {
            queryBuilder=QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.insertDevice"), true)
                    .setObject(device);
            queryBuilder.executeUpdate();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally {
            queryBuilder.closeConnection();
        }

    }
    public  long getDevices(Device device) throws SQLException {
        long deviceId = 0l;
        QueryBuilder queryBuilder = null;
        try {
            queryBuilder = QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.selectDevicesReg"))
                    .setString("uniqueId", device.getUniqueId());

            ResultSet resultSet = queryBuilder.getStatement().executeQuery();
            if (resultSet.next()) {
                deviceId = resultSet.getLong("id");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        finally{
            queryBuilder.closeConnection();
        }
        return deviceId;
    }
    public void addLocation(Position position) throws SQLException {
        QueryBuilder queryBuilder = null;
        try
        {
            queryBuilder=QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.insertPosition"));
            queryBuilder.setDate("now", new Date());
            queryBuilder.setObject(position);
            queryBuilder.executeUpdate();
        }
        catch (Exception ee)
        {
            ee.printStackTrace();
        }
        finally{
            queryBuilder.closeConnection();
        }
    }
    public  int isActive(String mobileno) throws SQLException {
        int isActive = 0;
        QueryBuilder queryBuilder = null;
        try {
            queryBuilder = QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.selectDevicesReg"))
                    .setString("uniqueId", mobileno);

            ResultSet resultSet = queryBuilder.getStatement().executeQuery();
            if (resultSet.next()) {
                isActive = resultSet.getInt("isactive");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        finally{
            queryBuilder.closeConnection();
        }
        return isActive;
    }
    public String getPositions(long deviceId) throws SQLException {
        QueryBuilder queryBuilder = null;
        JSONObject jsonObj = new JSONObject();
        try {
            queryBuilder=QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.selectPositionsMob"));
            queryBuilder.setLong("deviceId", deviceId);
            //queryBuilder.setLong("deviceId", deviceId);
            ResultSet resultSet =queryBuilder.getStatement().executeQuery();

            if(resultSet.next())
            {

                jsonObj.put("latitude", resultSet.getString("latitude"));
                jsonObj.put("longitude", resultSet.getString("longitude"));
                jsonObj.put("altitude", resultSet.getString("altitude"));
                jsonObj.put("speed", resultSet.getString("speed"));
                jsonObj.put("heading", resultSet.getString("course"));
                jsonObj.put("devicetime", resultSet.getString("devicetime"));

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            queryBuilder.closeConnection();
        }
        return jsonObj.toString();
    }
    public void updateNotification(int fromDevice,int toDevice,String notifyBy) throws SQLException {
        QueryBuilder queryBuilder=null;
        try {
            int sms=0,not=0;
            if(notifyBy.equalsIgnoreCase("sms"))sms=1;
            else not=1;
            queryBuilder=QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.updateUserStatus"))
                    .setInteger("isSMSSent",sms);
            queryBuilder=queryBuilder.setInteger("notiRequestStatus", not);
            queryBuilder=queryBuilder.setInteger("from", fromDevice);
            queryBuilder=queryBuilder.setInteger("to",toDevice);
            queryBuilder.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            queryBuilder.closeConnection();
        }
    }
    public String getFollowers(long deviceId) throws SQLException {
        QueryBuilder queryBuilder = null;

        JSONArray arry = new JSONArray();
        try {
            queryBuilder=QueryBuilder.create(dataManager.getDataSource(), dataManager.getQuery("database.getfollowers"));
            queryBuilder.setLong("deviceId", deviceId);
            //queryBuilder.setLong("deviceId", deviceId);
            ResultSet resultSet =queryBuilder.getStatement().executeQuery();

            while (resultSet.next())
            {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("Display Name", resultSet.getString("NAME"));
                jsonObj.put("Mobile No", resultSet.getString("uniqueid"));
                arry.put(jsonObj);

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            queryBuilder.closeConnection();
        }
        return arry.toString();
    }
}
