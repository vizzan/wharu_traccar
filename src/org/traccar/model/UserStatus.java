package org.traccar.model;

import java.util.List;

/**
 * Created by Usman on 11-07-2016.
 */
public class UserStatus {
    private String isUserRegisterd, isSmsSent,  isInFav, shareLocationStatus;
    private List<Position> positions;
    private String from, to;
    int notificationRequestStatus;
    private String isRequestMakerExist;

    public String getIsUserRegisterd() {
        return isUserRegisterd;
    }

    public void setIsUserRegisterd(String isUserRegisterd) {
        this.isUserRegisterd = isUserRegisterd;
    }

    public String getIsSmsSent() {
        return isSmsSent;
    }

    public void setIsSmsSent(String isSmsSent) {
        this.isSmsSent = isSmsSent;
    }

    public int getNotificationRequestStatus() {
        return notificationRequestStatus;
    }

    public void setNotificationRequestStatus(int notificationRequestStatus) {
        this.notificationRequestStatus = notificationRequestStatus;
    }

    public String getIsInFav() {
        return isInFav;
    }

    public void setIsInFav(String isInFav) {
        this.isInFav = isInFav;
    }

    public String getShareLocationStatus() {
        return shareLocationStatus;
    }

    public void setShareLocationStatus(String shareLocationStatus) {
        this.shareLocationStatus = shareLocationStatus;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getIsRequestMakerExist() {
        return isRequestMakerExist;
    }

    public void setIsRequestMakerExist(String isRequestMakerExist) {
        this.isRequestMakerExist = isRequestMakerExist;
    }
}
